#!/usr/bin/perl

package Debian::Debhelper::Buildsystem::vlang;

use strict;
use warnings;
use base 'Debian::Debhelper::Buildsystem';

sub DESCRIPTION {
  "V Language"
}

sub check_auto_buildable {
  my $this = shift;
  if (-f $this->get_sourcepath("v.mod")) {
    return 1;
  }
  return 0;
}

sub new {
  my $class = shift;
  my $this = $class->SUPER::new(@_);
  $this->enforce_in_source_building();
  set_vflags();
  return $this;
}

sub set_vflags {
  $ENV{'VFLAGS'} = "-cc $ENV{'cc'} -cflags $ENV{'CFLAGS'} -prod";
  if (defined $ENV{DH_VERBOSE} && $ENV{DH_VERBOSE} ne "") {
    $ENV{'VFLAGS'} = "-cc $ENV{'cc'} -cflags $ENV{'CFLAGS'} -prod -showcc -stats -v";
  }
}

sub build {
  my $this = shift;
  $this->doit_in_sourcedir("dh_vlang", "--build", @_);
}

sub test {
  my $this = shift;
  $this->doit_in_sourcedir("dh_vlang", "--test", @_);
}

1;
