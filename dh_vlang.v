import os

fn build() {
	os.system('v ' + os.getenv('VFLAGS') + ' .')
}

fn test() {
	os.system('v ' + os.getenv('VFLAGS') + ' test .')
}

fn main() {
	if '--build' in os.args {
		build()
	} else if '--test' in os.args {
		test()
	}
}
